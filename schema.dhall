let Schema =
    -- | define a configuration schema.
      { Type =
          { Host : Text
          , Port : Natural
          , HTTP :
              { Domain : Text
              , Secure : Bool
              , AutoTLS : Bool
              , TLSKeyPath : Text
              , TLSCertKeyPath : Text
              , HSTSMaxAge : Natural
              , ContentSecurityPolicy : Text
              , RateLimit : Natural
              , Gzip : Natural
              , Timeout : Natural
              }
          , Mailer :
              { Enabled : Bool
              , Protocol : Text
              , SMTPAddr : Text
              , SMTPPort : Natural
              , ForceTrustServerCert : Bool
              , EnableHELO : Bool
              , HELOHostname : Text
              , Auth : Text
              , From : Text
              , User : Text
              , Password : Text
              , SubjectPrefix : Text
              , SendPlainText : Bool
              }
          , LiveMode : Bool
          , DevelMode : Bool
          , AppPath : Text
          , Session :
              { CookieName : Text
              , CookieAuthSecret : Text
              , CookieEncrSecret : Text
              , MaxAge : Natural
              }
          , Logger : { JSON : Bool, Fmt : Optional Text }
          , Init : { CreateAdmin : Bool, AdminPassword : Text }
          , Registration : { Allowed : Bool }
          }
      , default =
          -- | have sane defaults.
          { Host = ""
          , Port = 3000
          , HTTP =
            { Domain = ""
            , Secure = False
            , AutoTLS = False
            , TLSKeyPath = ""
            , TLSCertKeyPath = ""
            , HSTSMaxAge = 0
            , ContentSecurityPolicy = ""
            , RateLimit = 0
            , Gzip = 0
            , Timeout = 0
            }
          , Mailer =
            { Enabled = False
            , Protocol = "smtps"
            , SMTPAddr = ""
            , SMTPPort = 465
            , ForceTrustServerCert = False
            , EnableHELO = False
            , HELOHostname = ""
            , Auth = ""
            , From = ""
            , User = ""
            , Password = ""
            , SubjectPrefix = "pcmt - "
            , SendPlainText = True
            }
          , LiveMode =
              -- | LiveMode controls whether the application looks for
              -- | directories "assets" and "templates" on the filesystem or
              -- | in its bundled Embed.FS.
              False
          , DevelMode = False
          , AppPath =
              -- | AppPath specifies where the program looks for "assets" and
              -- | "templates" in case LiveMode is True.
              "."
          , Session =
            { CookieName = "pcmt_session"
            , CookieAuthSecret = ""
            , CookieEncrSecret = ""
            , MaxAge = 3600
            }
          , Logger = { JSON = True, Fmt = None Text }
          , Init =
            { CreateAdmin =
                -- | if this is True, attempt to create a user with admin
                -- | privileges with the password specified below (or better -
                -- | overriden); it fails if users already exist in the DB.
                False
            , AdminPassword =
                -- | used for the first admin, forced change on first login.
                "50ce50fd0e4f5894d74c4caecb450b00c594681d9397de98ffc0c76af5cff5953eb795f7"
            }
          , Registration.Allowed = True
          }
      }

in  Schema

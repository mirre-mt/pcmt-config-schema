let Prelude =
    {- this will have to get bumped when prelude is updated -}
      ./prelude.dhall
        sha256:a6036bc38d883450598d1de7c98ead113196fe2db02e9733855668b18096f07b

let Schema =
        ./schema.dhall
          sha256:c2e9212c3d2d45b6706abbe96c5af21dabc0555ecf8e5b9eb824d479b71b88d1
      ? ./schema.dhall

let Schema/validate
    : Schema.Type -> Type
    =
      -- | define validation.
      \(c : Schema.Type) ->
        let expected = { validPort = True, validSMTPPort = True }

        let actual =
              { validPort =
                  -- | make sure port number belongs to the <1;65535> range.
                      Prelude.Natural.lessThanEqual 1 c.Port
                  &&  Prelude.Natural.lessThanEqual c.Port 65535
              , validSMTPPort =
                  if    c.Mailer.Enabled == False
                  then  True
                  else      Prelude.Natural.lessThanEqual 1 c.Mailer.SMTPPort
                        &&  Prelude.Natural.lessThanEqual
                              c.Mailer.SMTPPort
                              65535
              }

        in  expected === actual

in  Schema/validate

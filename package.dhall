{ Schema =
      ./schema.dhall
        sha256:c2e9212c3d2d45b6706abbe96c5af21dabc0555ecf8e5b9eb824d479b71b88d1
    ? ./schema.dhall
, Schema/validate =
      ./validate.dhall
        sha256:b549161c382eab83cc2c0063bd0ac0472b51b78687e4aa36f686244cf4a0a1b1
    ? ./validate.dhall
, Schema/version =
      ./version.dhall
        sha256:b11c1bdc95f7472fe925c3dd118a4032f332a09b7ddc2e493c8411549f1671e2
    ? ./version.dhall
, Prelude =
    ./prelude.dhall
      sha256:a6036bc38d883450598d1de7c98ead113196fe2db02e9733855668b18096f07b
}

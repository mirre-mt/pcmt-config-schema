# [pcmt-config-schema](https://git.dotya.ml/mirre-mt/pcmt-config-schema)

this repo is part of the [`pcmt`](https://git.dotya.ml/mirre-mt/pcmt/) project
and only hosts a [Dhall](https://dhall-lang.org/) configuration schema that has
been broken out of `pcmt` to allow for a more straight-forward schema version
management.

each release is tagged, which should enable identifying and isolating breaking
changes easily.

todo: migrate certain attributes to [enums](https://stackoverflow.com/questions/55316378/how-to-handle-enums-in-dhall).

### LICENSE
AGPL-3.0-only (see [LICENSE](LICENSE) for details).

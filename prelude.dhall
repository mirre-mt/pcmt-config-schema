{-
the env env will only be resolved if this file is imported locally, i.e. for
local development.
for remote scenarios the env will fall back to the integrity-protected import.
-}
  env:DHALL_PRELUDE
? https://prelude.dhall-lang.org/v20.2.0/package.dhall
    sha256:a6036bc38d883450598d1de7c98ead113196fe2db02e9733855668b18096f07b
